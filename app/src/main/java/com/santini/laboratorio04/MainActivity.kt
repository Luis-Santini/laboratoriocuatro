package com.santini.laboratorio04

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.santini.laboratorio04.adapter.AgendaAdapter
import com.santini.laboratorio04.model.Agenda
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.items.view.*

class MainActivity : AppCompatActivity() {
    var listaAgenda = mutableListOf<Agenda>()
    lateinit var adapter: AgendaAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        permisos()
        loadData()
        configurarAdapter()
    }

    fun permisos() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CALL_PHONE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.CALL_PHONE
                )
            ) {
                Toast.makeText(this, "Por favor active los permisos de llamada", Toast.LENGTH_SHORT)
                    .show()
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.CALL_PHONE),
                    42
                )
            }
        }
    }

    private fun loadData() {
        listaAgenda.add(Agenda("Alejandro", "Doctor", "alejandro@gmail.com", "3516547958"))
        listaAgenda.add(Agenda("Arturo", "Ing. Agronomo", "arturo@gmail.com", "3516547947"))
        listaAgenda.add(Agenda("Carlos", "Albañil", "carlos@gmail.com", "3516547948"))
        listaAgenda.add(Agenda("José", "carpintero", "josé@gmail.com", "3516547956"))
        listaAgenda.add(Agenda("Lautaro", "Verdulero", "lautaro@gmail.com", "3516547949"))
        listaAgenda.add(Agenda("Luis", "Desarrollador ", "luis@gmail.com", "3516547959"))
        listaAgenda.add(Agenda("Marcos", "Veterinario", "marcos@gmail.com", "3516547946"))
        listaAgenda.add(Agenda("Pedro", "Arquitecto", "pedro@gmail.com", "3516547957"))
    }

    private fun configurarAdapter() {
        // se instancia el objeto
        adapter = AgendaAdapter(listaAgenda, this)
        rcAgenda.adapter = adapter
        rcAgenda.layoutManager = LinearLayoutManager(this)
    }
}