package com.santini.laboratorio04.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.santini.laboratorio04.R
import com.santini.laboratorio04.model.Agenda


class AgendaAdapter(val listAdapter: MutableList<Agenda>, val context: Context) :
    RecyclerView.Adapter<AgendaAdapter.AgendaAdapterViewHolder>() {
    // ViewHolder
    class AgendaAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvNombre: TextView = itemView.findViewById(R.id.tvnombre)
        var tvProfesion: TextView = itemView.findViewById(R.id.tvProfesion)
        var tvEmail: TextView = itemView.findViewById(R.id.tvEmail)
        var imbTelefono: ImageButton = itemView.findViewById(R.id.imbTelefono)
        var tvSiglaNombre: TextView = itemView.findViewById(R.id.tvSiglaNombre)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AgendaAdapterViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.items, parent, false)
        return AgendaAdapterViewHolder(view)
    }
    override fun getItemCount(): Int {
        return listAdapter.size
    }
    override fun onBindViewHolder(holder: AgendaAdapterViewHolder, position: Int) {
        val agenda: Agenda = listAdapter[position]
        holder.tvNombre.text = agenda.nombre
        holder.tvProfesion.text = agenda.profesion
        holder.tvEmail.text = agenda.email
        holder.tvSiglaNombre.text = agenda.nombre.substring(0, 1)
        var numero = agenda.telefono
        holder.imbTelefono.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_CALL,
                Uri.parse("tel:{$numero}")
            )
            context.startActivity(intent)
        }
    }
}