package com.santini.laboratorio04.model

data class Agenda (
    val nombre: String,
    val profesion: String,
    val email:String,
    val telefono:String)